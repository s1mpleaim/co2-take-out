package com.coco.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coco.annotation.AutoFill;
import com.coco.entity.Employee;
import com.coco.enumeration.OperationType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

/**
 * @Author: coco
 * @Date: 2023/8/29 16:36
 */

@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {

    @Override
    @AutoFill(value = OperationType.INSERT)
    int insert(Employee entity);


    @AutoFill(value = OperationType.UPDATE)
    @Update({"UPDATE Employee SET username=#{username}, name=#{name}, phone=#{phone}, sex=#{sex}, id_number=#{idNumber}, status=#{status}, update_time=#{updateTime}, update_user=#{updateUser} WHERE id=#{id} "})
    int updateEmployeeById(Employee entity);
}
