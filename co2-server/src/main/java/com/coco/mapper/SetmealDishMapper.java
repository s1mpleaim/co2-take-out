package com.coco.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coco.entity.SetmealDish;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: coco
 * @Date: 2023/9/8 14:00
 */
@Mapper
public interface SetmealDishMapper extends BaseMapper<SetmealDish> {



}
