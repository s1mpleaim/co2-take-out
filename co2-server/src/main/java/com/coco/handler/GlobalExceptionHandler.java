package com.coco.handler;

import com.coco.constant.MessageConstant;
import com.coco.exception.BaseException;
import com.coco.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLIntegrityConstraintViolationException;

/**
 * 全局异常处理器，处理项目中抛出的业务异常
 * @Author: coco
 * @Date: 2023/8/30 10:07
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 业务异常
     * @param e
     * @return
     */
    @ExceptionHandler
    public Result exceptionHandler(BaseException e){
        log.error("异常信息:{}", e.getMessage());
        return Result.error(e.getMessage());
    }

    /**
     * SQL异常 （名称{*name}加唯一约束抛出异常）
     * @param e
     * @return
     */
    @ExceptionHandler
    public Result exceptionHandler(SQLIntegrityConstraintViolationException e){

        String massage = e.getMessage();
        if (massage.contains("Duplicate entry")){
            String msg = MessageConstant.ACCOUNT_ALREADY_EXIST;
            log.info(msg);
            return Result.error(msg);
        }
        return Result.error(MessageConstant.UNKNOWN_ERROR);
    }
}
