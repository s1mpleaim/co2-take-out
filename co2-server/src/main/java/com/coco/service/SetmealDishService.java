package com.coco.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.coco.entity.SetmealDish;

/**
 * @Author: coco
 * @Date: 2023/9/8 14:03
 */

public interface SetmealDishService extends IService<SetmealDish> {
}
