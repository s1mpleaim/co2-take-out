package com.coco.dto;

import com.coco.entity.Setmeal;
import com.coco.entity.SetmealDish;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author: coco
 * @Date: 2023/9/16 3:31
 */
@Data
@ApiModel(description = "新增套餐传递参数")
public class SetmealDTO extends Setmeal {

    @ApiModelProperty("菜品集合")
    private List<SetmealDish> setmealDishes;

    @ApiModelProperty("分类名")
    private String categoryName;


}
