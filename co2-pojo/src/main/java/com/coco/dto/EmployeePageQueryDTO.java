package com.coco.dto;

import lombok.Data;

/**
 * 分页查询数据传输对象
 * @Author: coco
 * @Date: 2023/8/31 17:25
 */
@Data
public class EmployeePageQueryDTO {

    private String name;

    private int pageSize;

    private int page;

}
