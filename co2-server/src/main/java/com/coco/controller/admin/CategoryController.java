package com.coco.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.coco.constant.MessageConstant;
import com.coco.dto.CategoryDTO;
import com.coco.dto.CategoryPageQueryDTO;
import com.coco.entity.Category;
import com.coco.result.PageResult;
import com.coco.result.Result;
import com.coco.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: coco
 * @Date: 2023/9/2 16:22
 */
@Slf4j
@RestController
@RequestMapping("/admin/category")
@Api(tags = "分类相关接口")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @ApiOperation("根据id查询分类")
    @GetMapping("/getById/{id}")
    public Result<Category> select(@PathVariable Long id){
        log.info(" 查询分类id:{}", id);
        Category category = categoryService.getById(id);
        return Result.success(category);
    }

    @ApiOperation("分类分页查询")
    @GetMapping("/page")
    public Result<PageResult> page(CategoryPageQueryDTO categoryPageQueryDTO){
        log.info("查询分页{}", categoryPageQueryDTO);
        PageResult pageResult = categoryService.page(categoryPageQueryDTO);
        return Result.success(pageResult);
    }

    @ApiOperation("条件分类查询")
    @GetMapping("/list")
    public Result<List<Category>> list(Category category){
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        //添加分类条件: 1菜品 2套餐
        queryWrapper.eq(category.getType() != null, Category::getType, category.getType());
        //添加菜品分类排序条件
        queryWrapper.orderByAsc(Category::getSort).orderByAsc(Category::getUpdateTime);
        List<Category> list = categoryService.list(queryWrapper);
        return Result.success(list);
    }

    @ApiOperation("删除分类")
    @DeleteMapping("/delete")
    public Result delete(Long id){
        log.info("删除分类的id:{}", id);
        categoryService.removeById(id);
        return Result.success();
    }

    @ApiOperation("新增分类")
    @PostMapping("/save")
    public Result save(@RequestBody CategoryDTO categoryDTO){
        log.info("新增分类{}", categoryDTO);
        categoryService.save(categoryDTO);
        return Result.success(MessageConstant.ADD_INFO_SUCCESS);
    }

    @ApiOperation("修改分类")
    @PutMapping("/update")
    public Result update(@RequestBody CategoryDTO categoryDTO){
        log.info("修改分类{}", categoryDTO);
        categoryService.update(categoryDTO);
        return Result.success(MessageConstant.UPDATE_INFO_SUCCESS);
    }

    //TODO 待完成
    @ApiOperation("启用锁定分类")
    @PostMapping("/status/{status}")
    public Result<String> status(@PathVariable Integer status, Long id){
        return Result.success();
    }
}
