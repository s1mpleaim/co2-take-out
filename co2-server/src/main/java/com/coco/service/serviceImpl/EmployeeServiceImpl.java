package com.coco.service.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coco.constant.MessageConstant;
import com.coco.constant.PasswordConstant;
import com.coco.constant.StatusConstant;
import com.coco.context.BaseContext;
import com.coco.dto.EmployeeDTO;
import com.coco.dto.EmployeeLoginDTO;
import com.coco.dto.EmployeePageQueryDTO;
import com.coco.entity.Employee;
import com.coco.exception.*;
import com.coco.mapper.EmployeeMapper;
import com.coco.result.PageResult;
import com.coco.service.EmployeeService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author: coco
 * @Date: 2023/8/29 16:37
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public Employee login(EmployeeLoginDTO employeeLoginDTO) {
        String username = employeeLoginDTO.getUsername();
        String password = employeeLoginDTO.getPassword();

        //1.账户是否存在
        if (username==null || password==null) {
            throw new AccountOrPasswordIsNullException(MessageConstant.ACCOUNT_OR_PASSWORD_IS_NULL);
        }
        //2.根据用户名查询数据库中的数据
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", username);
        Employee employee = employeeMapper.selectOne(queryWrapper);
        //3.处理各种异常情况（用户名不存在、密码不正确、账户被锁定）
        if (employee == null){
            //账户不存在
            throw new AccountNotFoundException(MessageConstant.ACCOUNT_NOT_FOUND);
        }
        // 完成前端传输回来的明文进行md5加密
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        if (!password.equals(employee.getPassword())){
            //密码错误
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }
        if (employee.getStatus() == StatusConstant.DISABLE){
            //账户被锁定
            throw new AccountLockedException(MessageConstant.ACCOUNT_LOCKED);
        }

        return employee;
    }

    @Override
    public void save(EmployeeDTO employeeDTO) {
        Employee employee = new Employee();
        //对象属性拷贝
        BeanUtils.copyProperties(employeeDTO, employee);
        //设置状态为启用 Status=1
        employee.setStatus(StatusConstant.ENABLE);
        //设置密码默认为123456
        employee.setPassword(DigestUtils.md5DigestAsHex(PasswordConstant.DEFAULT_PASSWORD.getBytes()));
//        已设置自动填充
//        记录当前创建时间和修改时间
//        employee.setCreateTime(LocalDateTime.now());
//        employee.setUpdateTime(LocalDateTime.now());
//        设置当前创建人id和修改人id
//        employee.setCreateUser(BaseContext.getThreadLocalId());
//        employee.setUpdateUser(BaseContext.getThreadLocalId());
        // 插入数据
        employeeMapper.insert(employee);
    }

    // TODO 后期需要屏蔽密码
    @Override
    public PageResult page(EmployeePageQueryDTO employeePageQueryDTO) {
        //构造分页构造器
        Page pageInfo = new Page(employeePageQueryDTO.getPage(), employeePageQueryDTO.getPageSize());
        //添加过滤条件
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.like(StringUtils.isNotEmpty(employeePageQueryDTO.getName()), Employee::getName, employeePageQueryDTO.getName());
        //执行查询
        employeeMapper.selectPage(pageInfo,queryWrapper);
        //封装
        Long total = pageInfo.getTotal();
        List<Employee> records = pageInfo.getRecords();
        return new PageResult(total,records);
    }

    @Override
    public void startOrStop(Integer status, Long id) {
        //判断状态是否正确
        if (!status.equals(StatusConstant.DISABLE) && !status.equals(StatusConstant.ENABLE)){
            throw new IllegalStatusException(StatusConstant.ILLEGAL_STATUS);
        }
        //使用builder构造器
        Employee employee = Employee.builder()
                .status(status)
                .id(id)
                .build();
        //添加过滤条件
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(Employee::getId,id);
        //判断员工id是否正确
        if (employee == null){
            throw new AccountNotFoundException(MessageConstant.ACCOUNT_NOT_FOUND);
        }
        //执行查询
        employeeMapper.update(employee,queryWrapper);
    }

    @Override
    public void update(EmployeeDTO employeeDTO) {
        Employee employee = new Employee();
        //对象属性拷贝
        BeanUtils.copyProperties(employeeDTO, employee);
        //设置状态为启用 Status=1
        employee.setStatus(StatusConstant.ENABLE);
        employeeMapper.updateEmployeeById(employee);
    }
}
