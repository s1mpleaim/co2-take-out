package com.coco.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * AliOss配置属性
 * @Author: coco
 * @Date: 2023/9/4 21:00
 */

@Component
@ConfigurationProperties(prefix = "coco.alioss")
@Data
public class AliOssProperties {

    private String endpoint;
    private String accessKeyId;
    private String accessKeySecret;
    private String bucketName;

}
