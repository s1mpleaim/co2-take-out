package com.coco.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: coco
 * @Date: 2023/9/7 22:08
 */
@Data
public class DishPageQueryDTO {

    private int page;

    private int pageSize;

    @ApiModelProperty("菜品名称")
    private String name;

    @ApiModelProperty("分类id")
    private Integer categoryId;

    @ApiModelProperty("0禁用 1启用")
    private Integer status;

}
