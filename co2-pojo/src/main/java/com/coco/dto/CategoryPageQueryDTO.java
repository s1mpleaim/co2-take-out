package com.coco.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: coco
 * @Date: 2023/9/2 16:08
 */
@Data
@ApiModel(description = "分类传递的参数")
public class CategoryPageQueryDTO implements Serializable {

    @ApiModelProperty("分类名称")
    private String name;

    @ApiModelProperty("分类类型：1.菜品分类 2.套餐分类")
    private Integer type;

    private int page;

    private int pageSize;

}
