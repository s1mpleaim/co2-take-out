package com.coco.exception;

/**
 * 密码异常
 * @Author: coco
 * @Date: 2023/8/30 6:20
 */
public class PasswordErrorException extends BaseException {
    public PasswordErrorException(){
    }

    public PasswordErrorException(String msg) {
        super(msg);
    }
}
