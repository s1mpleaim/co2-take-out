package com.coco.enumeration;

/**
 * 数据库操作类型
 * @Author: coco
 * @Date: 2023/9/4 11:40
 */
public enum OperationType {

    /**
     * 修改
     */
    UPDATE,

    /**
     * 插入
     */
    INSERT

}
