package com.coco.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: coco
 * @Date: 2023/8/31 16:16
 */
@Configuration
public class MybatisPlusConfig {

    /**
     * mybatis-plus 引入分页插件（官方推荐的最新引入方式）
     * @return
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        //DbType  数据库类型
        PaginationInnerInterceptor page = new PaginationInnerInterceptor(DbType.MYSQL);
        //单次查询最大的数量   如果我查25条，返回还是10条。
        page.setMaxLimit(10L);
        //溢出总页数后是否做处理（默认不做，true表示做处理，回到首页） false  继续请求
        page.setOverflow(false);
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        //设置数据库类型为mysql
        interceptor.addInnerInterceptor(page);
        return interceptor;
    }
}
