package com.coco.constant;

/**
 * 公共字段自动填充相关常量
 * @Author: coco
 * @Date: 2023/9/4 14:32
 */
public class AutoFillConstant {

    //setter
    public static final String SET_CREATE_TIME = "setCreateTime";
    public static final String SET_UPDATE_TIME = "setUpdateTime";
    public static final String SET_CREATE_USER = "setCreateUser";
    public static final String SET_UPDATE_USER = "setUpdateUser";

    //属性名
    public static final String CREATE_TIME = "createTime";
    public static final String UPDATE_TIME = "updateTime";
    public static final String CREATE_USER = "createUser";
    public static final String UPDATE_USER = "updateUser";
}
