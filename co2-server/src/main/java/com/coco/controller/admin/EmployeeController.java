package com.coco.controller.admin;

import com.coco.constant.JwtClaimsConstant;
import com.coco.constant.MessageConstant;
import com.coco.dto.EmployeeDTO;
import com.coco.dto.EmployeeLoginDTO;
import com.coco.dto.EmployeePageQueryDTO;
import com.coco.entity.Employee;
import com.coco.properties.JwtProperties;
import com.coco.result.PageResult;
import com.coco.result.Result;
import com.coco.service.EmployeeService;
import com.coco.utils.JwtUtil;
import com.coco.vo.EmployeeLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;


/**
 * @Author: coco
 * @Date: 2023/8/29 16:40
 */
@Slf4j
@RestController
@RequestMapping("/admin/employee")
@Api(tags = "员工相关接口")
public class EmployeeController {

    @Autowired
    private JwtProperties jwtProperties;
    @Autowired
    private EmployeeService employeeService;

    @ApiOperation("员工登录")
    @PostMapping("/login")
    public Result<EmployeeLoginVO> login(@RequestBody EmployeeLoginDTO employeeLoginDTO){
        log.info("员工登录：{}", employeeLoginDTO);

        Employee employee = employeeService.login(employeeLoginDTO);

        //生成token
        Map<String, Object> claims = new HashMap<>();
        claims.put(JwtClaimsConstant.EMP_ID, employee.getId());
        String token = JwtUtil.createJWT(
                jwtProperties.getAdminSecretKey(),
                jwtProperties.getAdminTtl(),
                claims);

        EmployeeLoginVO employeeLoginVO = EmployeeLoginVO.builder()
                .id(employee.getId())
                .userName(employee.getUsername())
                .name(employee.getName())
                .token(token)
                .build();

        return Result.success(employeeLoginVO);
    }

    @ApiOperation("员工登出")
    @PostMapping("/logout")
    public Result<String> logout(){
        return Result.success();
    }

    @ApiOperation("新增员工")
    @PostMapping("/save")
    public Result save(@RequestBody EmployeeDTO employeeDTO){
        log.info("新增员工:{}", employeeDTO);
        employeeService.save(employeeDTO);
        return Result.success(MessageConstant.ADD_INFO_SUCCESS);
    }

    //TODO 待完成service -> 屏蔽密码
    @ApiOperation("根据id查询员工")
    @GetMapping("/getById/{id}")
    public Result<Employee> select(@PathVariable Long id){
        log.info(" 查询员工id:{}", id);
        Employee employee = employeeService.getById(id);
        return Result.success(employee);
    }

    @ApiOperation("编辑员工信息")
    @PutMapping("/update")
    public Result update(@RequestBody EmployeeDTO employeeDTO){
        log.info("编辑员工:{}", employeeDTO);
        employeeService.update(employeeDTO);
        return Result.success(MessageConstant.UPDATE_INFO_SUCCESS);
    }

    @ApiOperation("员工分页查询")
    @GetMapping("/list")
    public Result<PageResult> page(EmployeePageQueryDTO employeePageQueryDTO){
        log.info("查询员工:{}", employeePageQueryDTO);
        PageResult pageResult = employeeService.page(employeePageQueryDTO);
        return Result.success(pageResult);
    }


    @ApiOperation("员工账户启用禁用")
    @PostMapping("/status/{status}")
    public Result startOrStop(@PathVariable Integer status, Long id){
        log.info("员工id:{}，变更状态:{}",id,status);
        employeeService.startOrStop(status,id);
        return Result.success();
    }

    @ApiOperation("删除分类")
    @DeleteMapping("/delete")
    public Result delete(Long id){
        log.info("删除员工的id:{}", id);
        employeeService.removeById(id);
        return Result.success();
    }
}
