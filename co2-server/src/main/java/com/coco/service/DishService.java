package com.coco.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.coco.dto.DishDTO;
import com.coco.dto.DishPageQueryDTO;
import com.coco.entity.Dish;
import com.coco.result.PageResult;
import com.coco.vo.DishVO;

import java.util.List;

/**
 * @Author: coco
 * @Date: 2023/9/5 16:43
 */
public interface DishService extends IService<Dish> {

    /**
     * 新增菜品和对应的口味
     * @param dishDTO
     */
    void saveWishFlavor(DishDTO dishDTO);

    /**
     * 菜品分页查询
     * @param dishPageQueryDTO
     * @return
     */
    PageResult pageQuery(DishPageQueryDTO dishPageQueryDTO);

    /**
     * 菜品批量删除
     * @param ids
     */
    void deleteBatch(List<Long> ids);

    /**
     * 根据id查询菜品和对应口味数据
     * @param id
     * @return
     */
    DishVO getByIDWithFlavor(Long id);

    /**
     * 修改菜品信息和对应口味信息
     * @param dishDTO
     */
    void updateWithFlavor(DishDTO dishDTO);

    /**
     * 根据分类id查询对应分类的菜品数据
     * @param dish
     */
    List<Dish> getByCategoryId(Dish dish);
}
