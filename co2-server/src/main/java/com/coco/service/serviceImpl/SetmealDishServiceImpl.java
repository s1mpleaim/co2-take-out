package com.coco.service.serviceImpl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coco.entity.SetmealDish;
import com.coco.mapper.SetmealDishMapper;
import com.coco.service.SetmealDishService;
import org.springframework.stereotype.Service;

/**
 * @Author: coco
 * @Date: 2023/9/8 14:04
 */
@Service
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper, SetmealDish> implements SetmealDishService {
}
