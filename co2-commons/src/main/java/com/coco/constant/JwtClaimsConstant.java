package com.coco.constant;

/**
 * @Author: coco
 * @Date: 2023/8/29 11:33
 */
public class JwtClaimsConstant {

    public static final String EMP_ID = "empId";
    public static final String USER_ID = "userId";
    public static final String PHONE = "phone";
    public static final String USERNAME = "username";
    public static final String NAME = "name";

}
