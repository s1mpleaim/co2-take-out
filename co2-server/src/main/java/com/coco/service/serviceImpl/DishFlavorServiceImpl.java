package com.coco.service.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coco.entity.DishFlavor;
import com.coco.mapper.DishFlavorMapper;
import com.coco.service.DishFlavorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: coco
 * @Date: 2023/9/7 14:46
 */
@Service
public class DishFlavorServiceImpl extends ServiceImpl<DishFlavorMapper, DishFlavor> implements DishFlavorService {

    @Autowired
    private DishFlavorMapper dishFlavorMapper;

    @Override
    public void deleteByDishId(Long dishId) {
        dishFlavorMapper.deleteByDishId(dishId);
    }

    @Override
    public List<DishFlavor> getByDishId(Long id) {
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        List<DishFlavor> dishFlavors = dishFlavorMapper.selectList(queryWrapper.like(DishFlavor::getDishId, id));
        return dishFlavors;
    }
}
