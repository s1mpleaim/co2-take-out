package com.coco.result;

import lombok.Data;

import java.io.Serializable;

/**
 * 后端统一返回结果
 * @Author: coco
 * @Date: 2023/8/29 16:00
 */
@Data
public class Result<T> implements Serializable {

    private Integer code;   //编码：成功返回1，失败返回 0 或 其他
    private String msg; //错误信息
    private T data; //数据

    public static <T> Result<T> success() {
        Result<T> r = new Result<T>();
        r.code = 1;
        r.msg = "success";
        return r;
    }

    public static <T> Result<T> success(T object) {
        Result<T> r = new Result<T>();
        r.data = object;
        r.code = 1;
        r.msg = "success";
        return r;
    }

    public static <T> Result<T> error(String msg) {
        Result r = new Result();
        r.msg = msg;
        r.code = 0;
        return r;
    }

}
