package com.coco.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 封装分页查询结果
 * @Author: coco
 * @Date: 2023/8/31 10:00
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageResult implements Serializable {

    private Long total; //总记录数

    private List records;   //当前页数据集合
}
