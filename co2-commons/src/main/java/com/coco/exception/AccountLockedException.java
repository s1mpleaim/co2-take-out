package com.coco.exception;

/**
 * @Author: coco
 * @Date: 2023/8/30 6:30
 */
public class AccountLockedException extends BaseException{
    public AccountLockedException() {
    }

    public AccountLockedException(String msg) {
        super(msg);
    }

}
