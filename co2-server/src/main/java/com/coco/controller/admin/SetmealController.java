package com.coco.controller.admin;

import com.coco.constant.MessageConstant;
import com.coco.dto.SetmealDTO;
import com.coco.dto.SetmealPageQueryDTO;
import com.coco.result.PageResult;
import com.coco.result.Result;
import com.coco.service.SetmealDishService;
import com.coco.service.SetmealService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: coco
 * @Date: 2023/9/17 5:42
 */
@Slf4j
@RestController
@RequestMapping("/admin/setmeal")
@Api(tags = "套餐相关接口")
public class SetmealController {

    @Autowired
    SetmealService setmealService;
    @Autowired
    SetmealDishService setmealDishService;

    @PostMapping("/save")
    @ApiOperation("新增套餐")
    public Result<String> save(@RequestBody SetmealDTO setmealDTO){
        log.info("套餐信息: {}",setmealDTO);
        setmealService.saveWithDish(setmealDTO);
        return Result.success(MessageConstant.ADD_INFO_SUCCESS);
    }

    @GetMapping("/page")
    @ApiOperation("套餐分页查询")
    public Result<PageResult> page(SetmealPageQueryDTO setmealPageQueryDTO){
        log.info("套餐分页查询:{}", setmealPageQueryDTO);
        PageResult pageResult = setmealService.pageQuery(setmealPageQueryDTO);
        return Result.success(pageResult);
    }

}
