package com.coco.exception;

/**
 * @Author: coco
 * @Date: 2023/8/30 6:15
 */
public class AccountNotFoundException extends BaseException {
    public AccountNotFoundException() {
    }

    public AccountNotFoundException(String msg) {
        super(msg);
    }
}
