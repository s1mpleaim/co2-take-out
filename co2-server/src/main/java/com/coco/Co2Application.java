package com.coco;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @Author: coco
 * @Date: 2023/8/29 16:50
 */

@SpringBootApplication
//@EnableTransactionManagement    //开启注释方式的事务管理
@Slf4j
public class Co2Application {
    public static void main(String[] args) {
        SpringApplication.run(Co2Application.class, args);
        log.info("服务已开启。");
    }
}
