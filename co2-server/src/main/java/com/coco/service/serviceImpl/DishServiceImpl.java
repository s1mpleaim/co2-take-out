package com.coco.service.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coco.constant.MessageConstant;
import com.coco.constant.StatusConstant;
import com.coco.dto.DishDTO;
import com.coco.dto.DishPageQueryDTO;
import com.coco.entity.Category;
import com.coco.entity.Dish;
import com.coco.entity.DishFlavor;
import com.coco.entity.SetmealDish;
import com.coco.exception.DeletionNotAllowedException;
import com.coco.mapper.DishMapper;
import com.coco.result.PageResult;
import com.coco.service.CategoryService;
import com.coco.service.DishFlavorService;
import com.coco.service.DishService;
import com.coco.service.SetmealDishService;
import com.coco.vo.DishVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: coco
 * @Date: 2023/9/5 16:44
 */
@Service
public class DishServiceImpl extends ServiceImpl<DishMapper,Dish> implements DishService {

    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private DishFlavorService dishFlavorService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private SetmealDishService setmealDishService;

    @Override
    @Transactional
    public void saveWishFlavor(DishDTO dishDTO) {
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO,dish);
        //向菜品表插入1条数据
        dishMapper.insert(dish);
        Long dishId = dish.getId();  //菜品id
        List<DishFlavor> flavors = dishDTO.getFlavors();
        if (flavors != null && flavors.size() > 0){
            //向口味表插入n条数据
            flavors.forEach(dishFlavor -> {
                dishFlavor.setDishId(dishId);
            });
            dishFlavorService.saveBatch(flavors);
        }
    }

    @Override
    public PageResult pageQuery(DishPageQueryDTO dishPageQueryDTO) {
        Page pageInfo = new Page(dishPageQueryDTO.getPage(), dishPageQueryDTO.getPageSize());
        Page<DishVO> dishVOPage = new Page<>();
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(dishPageQueryDTO.getName() != null, Dish::getName,dishPageQueryDTO.getName());
        queryWrapper.orderByDesc(Dish::getUpdateTime);
        dishMapper.selectPage(pageInfo, queryWrapper);
        //拷贝pageInfo里除records记录意外的其他参数
        BeanUtils.copyProperties(pageInfo, dishVOPage,"records");
        Long total = pageInfo.getTotal();
        List<Dish> records = pageInfo.getRecords();
        //TODO 后期维护如果菜品不属于任何分类情况
        List<DishVO> dishVOList = records.stream().map((item) -> {
            DishVO dishVO = new DishVO();
            BeanUtils.copyProperties(item, dishVO);
            Long categoryId = item.getCategoryId();
            Category category = categoryService.getById(categoryId);
            String categoryName = category.getName();
            dishVO.setCategoryName(categoryName);
            return dishVO;
        }).collect(Collectors.toList());
        dishVOPage.setRecords(dishVOList);
        return new PageResult(total, dishVOList);
    }

    @Override
    @Transactional //操作多个表时添加该注解保证数据一致性
    public void deleteBatch(List<Long> ids) {
        //判断当前菜品是否能删除--是否为起售中的菜品
        for (Long id : ids){
            Dish dish = dishMapper.selectById(id);
            if (dish.getStatus() == StatusConstant.ENABLE){
                //当前菜品处于起售状态,status=1
                throw new DeletionNotAllowedException(MessageConstant.DISH_ON_SALE);
            }
        }
        //判断当前菜品是否能删除--是否被套餐关联
        List<SetmealDish> setmealIds = setmealDishService.listByIds(ids);
        if (setmealIds != null && setmealIds.size() > 0){
            //当前菜品被套餐关联，不能删除
            throw new DeletionNotAllowedException(MessageConstant.CATEGORY_BE_RELATED_BY_DISH);
        }
        //删除菜品表菜品数据&&相关的口味数据
        for (Long id : ids){
            dishMapper.deleteById(id);
            dishFlavorService.deleteByDishId(id);
        }
    }

    @Override
    public DishVO getByIDWithFlavor(Long id) {
        //根据id查询菜品数据
        Dish dish = dishMapper.selectById(id);
        //根据id查询口味数据
        List<DishFlavor> dishFlavors = dishFlavorService.getByDishId(id);
        //封装vo
        DishVO dishVO = new DishVO();
        BeanUtils.copyProperties(dish,dishVO);
        dishVO.setFlavors(dishFlavors);
        return dishVO;
    }

    @Override
    public void updateWithFlavor(DishDTO dishDTO) {
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO, dish);
        //修改菜品信息
        dishMapper.updateById(dish);
        //删除原有口味信息
        dishFlavorService.deleteByDishId(dishDTO.getId());
        //插入口味信息
        List<DishFlavor> flavors = dishDTO.getFlavors();
        if (flavors != null && flavors.size() > 0){
            //向口味表插入n条数据
            flavors.forEach(dishFlavor -> {
                dishFlavor.setDishId(dishDTO.getId());
            });
            dishFlavorService.saveBatch(flavors);
        }
    }

    @Override
    public List<Dish> getByCategoryId(Dish dish) {
        //构造查询条件
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(dish.getCategoryId() != null, Dish::getCategoryId,dish.getCategoryId());
        //添加状态条件, status=1(起售)
        queryWrapper.eq(Dish::getStatus, StatusConstant.ENABLE);
        //添加排序条件
        queryWrapper.orderByDesc(Dish::getUpdateTime);
        List<Dish> list = dishMapper.selectList(queryWrapper);
        return list;
    }
}
