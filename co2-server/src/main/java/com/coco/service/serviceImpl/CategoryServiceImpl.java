package com.coco.service.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coco.constant.StatusConstant;
import com.coco.context.BaseContext;
import com.coco.dto.CategoryDTO;
import com.coco.dto.CategoryPageQueryDTO;
import com.coco.entity.Category;
import com.coco.mapper.CategoryMapper;
import com.coco.result.PageResult;
import com.coco.service.CategoryService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: coco
 * @Date: 2023/9/2 16:29
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public PageResult page(CategoryPageQueryDTO categoryPageQueryDTO) {
        Page pageInfo = new Page(categoryPageQueryDTO.getPage(),categoryPageQueryDTO.getPageSize());
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        //查询条件：分类名称，分类类型
        if (categoryPageQueryDTO.getType() != null){
            queryWrapper.like(StringUtils.isNotEmpty(categoryPageQueryDTO.getName()), Category::getName, categoryPageQueryDTO.getName())
                    .or()
                    .like(Category::getType, categoryPageQueryDTO.getType());
        }
        queryWrapper.like(StringUtils.isNotEmpty(categoryPageQueryDTO.getName()), Category::getName, categoryPageQueryDTO.getName());
        categoryMapper.selectPage(pageInfo, queryWrapper);
        Long total = pageInfo.getTotal();
        List<Category> records = pageInfo.getRecords();
        return new PageResult(total, records);
    }

    @Override
    public void save(CategoryDTO categoryDTO) {
        Category category = new Category();
        BeanUtils.copyProperties(categoryDTO, category);
        category.setStatus(StatusConstant.ENABLE);
        categoryMapper.insert(category);
    }

    @Override
    public void update(CategoryDTO categoryDTO) {
        Category category = new Category();
        BeanUtils.copyProperties(categoryDTO, category);
        categoryMapper.updateById(category);
    }


}
