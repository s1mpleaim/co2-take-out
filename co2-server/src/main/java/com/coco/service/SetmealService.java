package com.coco.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.coco.dto.SetmealDTO;
import com.coco.dto.SetmealPageQueryDTO;
import com.coco.entity.Setmeal;
import com.coco.result.PageResult;

/**
 * @Author: coco
 * @Date: 2023/9/17 5:45
 */
public interface SetmealService extends IService<Setmeal> {

    /**
     * 新增套餐信息，同时保存套餐和菜品关联关系
     * @param setmealDTO
     */
    void saveWithDish(SetmealDTO setmealDTO);

    /**
     *
     * @param setmealPageQueryDTO
     * @return
     */
    PageResult pageQuery(SetmealPageQueryDTO setmealPageQueryDTO);
}
