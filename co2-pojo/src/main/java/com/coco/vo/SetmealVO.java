package com.coco.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Author: coco
 * @Date: 2023/9/17 8:15
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SetmealVO implements Serializable {

    private Long id;
    //套餐名称
    private String name;
    //套餐分类id
    private Long categoryId;
    //套餐价格
    private BigDecimal price;
    //0 停售 1 起售
    private Integer status;
    //描述信息
    private String description;
    //图片
    private String image;
    //更新时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    //分类名称
    private String categoryName;
}
