package com.coco.exception;

/**
 * 账户或密码为空
 * @Author: coco
 * @Date: 2023/8/30 5:54
 */
public class AccountOrPasswordIsNullException extends BaseException {

    public AccountOrPasswordIsNullException() {
    }

    public AccountOrPasswordIsNullException(String msg) {
        super(msg);
    }


}
