package com.coco.exception;

/**
 * 状态异常
 * @Author: coco
 * @Date: 2023/9/2 6:45
 */
public class IllegalStatusException extends BaseException {
    public IllegalStatusException (){

    }

    public IllegalStatusException (String msg){
        super(msg);
    }
}
