package com.coco.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: coco
 * @Date: 2023/9/2 17:51
 */
@Data
public class CategoryDTO {

    private Long id;

    @ApiModelProperty("分类名称")
    private String name;

    @ApiModelProperty("排序，按照升序排序")
    private Integer sort;

    @ApiModelProperty("分类类型：1.菜品分类 2.套餐分类")
    private Integer type;

}
