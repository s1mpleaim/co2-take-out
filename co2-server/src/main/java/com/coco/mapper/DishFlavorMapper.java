package com.coco.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coco.entity.DishFlavor;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author: coco
 * @Date: 2023/9/7 14:17
 */
@Mapper
public interface DishFlavorMapper extends BaseMapper<DishFlavor> {


    /**
     * 根据菜品id删除对应的口味数据
     * @param dishId
     */
    @Delete("DELETE FROM dish_flavor WHERE dish_id = #{dishId}")
    void deleteByDishId(Long dishId);

}
