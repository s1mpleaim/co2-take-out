package com.coco.context;

/**
 * 线程公共类
 * @Author: coco
 * @Date: 2023/8/30 14:44
 */
public class BaseContext {

    public static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    public static void setThreadLocalId(Long id){
        threadLocal.set(id);
    }

    public static Long getThreadLocalId(){
        return threadLocal.get();
    }

    public static void removeThreadLocalId(){
        threadLocal.remove();
    }

}
