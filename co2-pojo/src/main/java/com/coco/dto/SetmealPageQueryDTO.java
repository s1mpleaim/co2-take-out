package com.coco.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: coco
 * @Date: 2023/9/17 8:08
 */
@Data
public class SetmealPageQueryDTO {

    private int page;

    private int pageSize;

    @ApiModelProperty("套餐名称")
    private String name;

    @ApiModelProperty("套餐分类id")
    private Integer categoryId;

    @ApiModelProperty("0禁用 1启用")
    private Integer status;

}
