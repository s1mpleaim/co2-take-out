package com.coco.exception;

/**
 * @Author: coco
 * @Date: 2023/8/30 5:55
 */
public class BaseException extends RuntimeException {
    public BaseException() {
    }

    public BaseException(String msg) {
        super(msg);
    }

}
