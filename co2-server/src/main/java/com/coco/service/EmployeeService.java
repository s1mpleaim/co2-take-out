package com.coco.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.coco.dto.EmployeeDTO;
import com.coco.dto.EmployeeLoginDTO;
import com.coco.dto.EmployeePageQueryDTO;
import com.coco.entity.Employee;
import com.coco.result.PageResult;

/**
 * @Author: coco
 * @Date: 2023/8/29 16:37
 */
public interface EmployeeService extends IService<Employee> {

    /**
     * 员工登录
     * @param employeeLoginDTO
     * @return
     */
    Employee login(EmployeeLoginDTO employeeLoginDTO);

    /**
     * 新增员工
     * @param employeeDTO
     */
    void save(EmployeeDTO employeeDTO);

    /**
     * 员工分页模糊查询
     * @param employeePageQueryDTO
     * @return
     */
    PageResult page(EmployeePageQueryDTO employeePageQueryDTO);

    /**
     * 启用禁用员工账户
     * @param status
     * @param id
     */
    void startOrStop(Integer status, Long id);

    /**
     * 编辑员工信息
     * @param employeeDTO
     */
    void update(EmployeeDTO employeeDTO);
}
