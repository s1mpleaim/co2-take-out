package com.coco.service.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coco.dto.SetmealDTO;
import com.coco.dto.SetmealPageQueryDTO;
import com.coco.entity.Category;
import com.coco.entity.Setmeal;
import com.coco.entity.SetmealDish;
import com.coco.mapper.SetmealMapper;
import com.coco.result.PageResult;
import com.coco.service.CategoryService;
import com.coco.service.SetmealDishService;
import com.coco.service.SetmealService;
import com.coco.vo.SetmealVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: coco
 * @Date: 2023/9/17 5:45
 */
@Service
public class SetmealServiceImpl  extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {

    @Autowired
    private SetmealMapper setmealMapper;
    @Autowired
    private SetmealDishService setmealDishService;
    @Autowired
    private CategoryService categoryService;

    @Override
    @Transactional
    public void saveWithDish(SetmealDTO setmealDTO) {
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDTO,setmeal);
        //保存套餐基本信息
        setmealMapper.insert(setmeal);
        //保存套餐和菜品关联关系
        Long setmealId = setmeal.getId();
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        if (setmealDishes != null && setmealDishes.size() >0){
            //向套餐菜品关系表插入n条数据
            setmealDishes.forEach(setmealDish -> {
                setmealDish.setSetmealId(setmealId);
            });
        }
        setmealDishService.saveBatch(setmealDishes);
    }

    @Override
    public PageResult pageQuery(SetmealPageQueryDTO setmealPageQueryDTO) {
        //与菜品分页类似
        Page pageInfo = new Page(setmealPageQueryDTO.getPage(), setmealPageQueryDTO.getPageSize());
        Page<SetmealVO> setmealVOPage = new Page<>();
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(setmealPageQueryDTO.getName() != null, Setmeal::getName, setmealPageQueryDTO.getName());
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);
        setmealMapper.selectPage(pageInfo, queryWrapper);
        BeanUtils.copyProperties(pageInfo, setmealVOPage, "records");
        Long total = pageInfo.getTotal();
        List<Setmeal> records = pageInfo.getRecords();
        List<SetmealVO> setmealVOList = records.stream().map((item) -> {
            SetmealVO setmealVO = new SetmealVO();
            BeanUtils.copyProperties(item, setmealVO);
            Long categoryId = item.getCategoryId();
            Category category = categoryService.getById(categoryId);
            String categoryName = category.getName();
            setmealVO.setCategoryName(categoryName);
            return setmealVO;
        }).collect(Collectors.toList());
        setmealVOPage.setRecords(setmealVOList);
        return new PageResult(total, setmealVOList);
    }
}
