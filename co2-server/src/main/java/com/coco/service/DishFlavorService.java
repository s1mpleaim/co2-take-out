package com.coco.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.coco.entity.DishFlavor;

import java.util.List;

/**
 * @Author: coco
 * @Date: 2023/9/7 14:45
 */
public interface DishFlavorService extends IService<DishFlavor> {

    /**
     * 根据菜品id删除对应的口味数据
     * @param dishId
     */
    void deleteByDishId(Long dishId);

    /**
     * 根据
     * @param id
     * @return
     */
    List<DishFlavor> getByDishId(Long id);
}
