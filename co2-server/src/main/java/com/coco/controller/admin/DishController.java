package com.coco.controller.admin;

import com.coco.constant.MessageConstant;
import com.coco.dto.DishDTO;
import com.coco.dto.DishPageQueryDTO;
import com.coco.entity.Dish;
import com.coco.result.PageResult;
import com.coco.result.Result;
import com.coco.service.DishService;
import com.coco.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: coco
 * @Date: 2023/9/5 16:13
 */
@Slf4j
@RestController
@RequestMapping("/admin/dish")
@Api(tags = "菜品相关接口")
public class DishController {

    @Autowired
    DishService dishService;

    @PostMapping("/save")
    @ApiOperation("新增菜品")
    public Result save(@RequestBody DishDTO dishDTO){
        log.info("新增菜品:{}",dishDTO.toString());
        dishService.saveWishFlavor(dishDTO);
        return Result.success(MessageConstant.ADD_INFO_SUCCESS);
    }

    @GetMapping("/page")
    @ApiOperation("菜品分页查询")
    public Result<PageResult> page(DishPageQueryDTO dishPageQueryDTO){
        log.info("菜品分页查询:{}", dishPageQueryDTO);
        PageResult pageResult = dishService.pageQuery(dishPageQueryDTO);
        return Result.success(pageResult);
    }

    @DeleteMapping("/delete")
    @ApiOperation("菜品批量删除")
    public Result delete(@RequestParam List<Long> ids){
        log.info("菜品批量删除:{}", ids);
        dishService.deleteBatch(ids);
        return Result.success();
    }

    @GetMapping("/getById/{id}")
    @ApiOperation("根据id查询菜品")
    public Result<DishVO> getById(@PathVariable Long id){
        log.info("根据id查询菜品:{}", id);
        DishVO dishVO = dishService.getByIDWithFlavor(id);
        return Result.success(dishVO);
    }

    @PutMapping("/update")
    @ApiOperation("修改菜品")
    public Result update(@RequestBody DishDTO dishDTO){
        log.info("修改菜品:{}",dishDTO.toString());
        dishService.updateWithFlavor(dishDTO);
        return Result.success(MessageConstant.UPDATE_INFO_SUCCESS);
    }

    @GetMapping("/list")
    @ApiOperation("根据分类id查询菜品")
    public Result<List<Dish>> list(Dish dish){
        log.info("根据分类id查询菜品:{}",dish.getCategoryId());
        List<Dish> list = dishService.getByCategoryId(dish);
        return Result.success(list);
    }

}
