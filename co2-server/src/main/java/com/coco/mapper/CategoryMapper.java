package com.coco.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coco.entity.Category;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: coco
 * @Date: 2023/9/2 16:28
 */
@Mapper
public interface CategoryMapper extends BaseMapper<Category> {
}
