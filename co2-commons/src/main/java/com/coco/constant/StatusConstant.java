package com.coco.constant;

/**
 * 状态常量，启用或者禁用
 * @Author: coco
 * @Date: 2023/8/30 6:25
 */
public class StatusConstant {

    //启用
    public static final Integer ENABLE = 1;

    //禁用
    public static final Integer DISABLE = 0;

    //状态异常
    public static final String ILLEGAL_STATUS = "非法状态";
}
