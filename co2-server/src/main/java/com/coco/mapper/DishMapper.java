package com.coco.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coco.entity.Dish;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: coco
 * @Date: 2023/9/5 16:44
 */
@Mapper
public interface DishMapper extends BaseMapper<Dish> {
}
