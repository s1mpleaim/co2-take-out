package com.coco.dto;

import com.coco.entity.DishFlavor;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: coco
 * @Date: 2023/9/7 13:52
 */
@Data
public class DishDTO {

    private Long id;

    @ApiModelProperty("菜品名称")
    private String name;

    @ApiModelProperty("菜品id")
    private Long categoryId;

    @ApiModelProperty("菜品名称")
    private BigDecimal price;

    @ApiModelProperty("菜品图片")
    private String image;

    @ApiModelProperty("描述信息")
    private String description;

    @ApiModelProperty("状态: 0停售 1起售")
    private Integer status;
    @ApiModelProperty("口味")
    private List<DishFlavor> flavors = new ArrayList<>();

}
