package com.coco.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.coco.constant.AutoFillConstant;
import com.coco.context.BaseContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * 自定义元数据对象处理器
 * @Author: coco
 * @Date: 2023/9/4 18:59
 */
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    /**
     * 插入操作，自动填充
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("公共字段自动填充[INSERT]...");
        metaObject.setValue(AutoFillConstant.CREATE_TIME, LocalDateTime.now());
        metaObject.setValue(AutoFillConstant.UPDATE_TIME, LocalDateTime.now());
        metaObject.setValue(AutoFillConstant.CREATE_USER, BaseContext.getThreadLocalId());
        metaObject.setValue(AutoFillConstant.UPDATE_USER, BaseContext.getThreadLocalId());
    }

    /**
     * 修改操作，自动填充
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("公共字段自动填充[UPDATE]...");
        metaObject.setValue(AutoFillConstant.UPDATE_TIME, LocalDateTime.now());
        metaObject.setValue(AutoFillConstant.UPDATE_USER, BaseContext.getThreadLocalId());
    }
}
