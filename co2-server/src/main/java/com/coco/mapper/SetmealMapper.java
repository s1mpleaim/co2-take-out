package com.coco.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coco.entity.Setmeal;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: coco
 * @Date: 2023/9/17 5:46
 */
@Mapper
public interface SetmealMapper extends BaseMapper<Setmeal> {
}
