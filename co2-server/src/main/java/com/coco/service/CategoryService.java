package com.coco.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.coco.dto.CategoryDTO;
import com.coco.dto.CategoryPageQueryDTO;
import com.coco.entity.Category;
import com.coco.result.PageResult;

/**
 * @Author: coco
 * @Date: 2023/9/2 16:29
 */
public interface CategoryService extends IService<Category> {

    /**
     * 分类分页查询
     * @param categoryPageQueryDTO
     * @return
     */
    PageResult page(CategoryPageQueryDTO categoryPageQueryDTO);

    /**
     * 新增分类
     * @param categoryDTO
     */
    void save(CategoryDTO categoryDTO);

    /**
     * 修改分类
     * @param categoryDTO
     */
    void update(CategoryDTO categoryDTO);
}
