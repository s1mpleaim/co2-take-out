package com.coco.exception;

/**
 * 删除异常
 * @Author: coco
 * @Date: 2023/9/8 12:20
 */
public class DeletionNotAllowedException extends BaseException {

    public DeletionNotAllowedException() {
    }

    public DeletionNotAllowedException(String msg) {
        super(msg);
    }

}
